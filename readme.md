## Xerini Interview Feedback from Hakan Karlidag

### General Notes

* Since I am not a native Kotlin developer and not too familiar with its build environment, at first I spent quite some time trying to get it working on VS Code. After realising the setup is taking too long, I had to move to IDEA and realise it is doing a lot of things automatically based on gradle settings and fixed a lot of my problems.
* Please note that the specified Gradle version (`5.4.1`) didn't work with my environment (`java: 16.0.1`, `macOS: 11.4`) right away. Since there was no specific version requirement mentioned, I changed `gradle/wrapper/gradle-wrapper.properties` file to target a newer version (`6.9`). I haven't added any new dependencies neither to the front-end nor to the back-end, so I believe you can revert it back to `5.4.1` without any problems.

### Notes on the code

* I have implemented all core and stretch tasks
* I have added quite a few comments in the code but I wanted to highlight a few of them also here:
    * Obviously reading/writing data from file system is an expensive operation and far from ideal. I have done a few improvements given the scope of the exercise. The file is being created when the the features are requested for the first time. In addition, I have utilized a flag to minimize the number of reads from the file; if the file hasn't been changed the system uses the local variable to get back to the user instead of reading from the file again and again.
    * Since this is a user-driven application I didn't see any need to check if the given coordinates are already in the list as the user might want to add multiple markers in the same coordinates. If this was a requirement, we could have used a distinction operation to remove replications, or even better, not add them in the first place.
    * I am not too familiar with Kotlin data types but I come across a note about mutable lists having a speed advantage over immutable lists when heavy iteration involved. Therefore, although this is probably a case-by-case situation, it might be good to look out for and dig deeper especially when we deal with high number of markers in a production build.
    * On the UI side, for best practice, normally you would want to check if the map click event is coming from a double-click or single-click action. Because when user tries to zoom-in using the double-click action we shouldn't be adding new markers to the map. However, since it is not in the scope of this exercise and also it will introduce a tradeoff by applying delays to the click events I left this check out.

-------------


## Xerini Interview Exercise

### Build Instructions

This project provides a simple server-side component built in Kotlin and a front-end
component built in React-JS/Typescript. 

To build the whole project, including the front-end, execute the following gradle command from the project directory:

```
$ ./gradlew clean build
```

To build just the front end, execute the following commands (note you'll need the latest version of node/npm from https://nodejs.org/en/)

```
$ cd ./ui
$ npm install
$ npm run-script build
```

To start the app, run the main function located in class `com.xerini.interview.App`

This will start the app and will serve the UI at http://localhost:7080/

### Brief

The current UI features a basic map based on the [OpenLayers](https://openlayers.org/) library.
The back-end features two REST endpoints that are defined in `com.xerini.interview.server.api.GeoJsonService`

The endpoint `GET /geo-json` is expected to return [GeoJson](https://en.wikipedia.org/wiki/GeoJSON) data to the front-end.

The endpoint `POST /geo-json/add` is expected to receive coordinates for a single point and persist these such that they are returned by subsequent calls to the `GET` endpoint

The first task is to add implementations for these two endpoints so that new data points can be added and then returned to the front-end. An in-memory persistence solution is fine for the purposes of this exercise.

The second task is to implement the function `onMapClick` in the file `ui/src/map-view.tsx` so that when the user clicks on the map, the location is stored in the rest API and the map data refreshed so that the updated point is displayed on the map.

### Stretch tasks

It's by no means mandatory but, if you have time, then one (or more) of the following tasks can be completed for extra marks:

* Update the front-end map to support clustering of points, see [here](https://openlayers.org/en/latest/examples/cluster.html) for further documentation.
* Change the persistence layer so that it writes and reads data from the file system.
* Add additional controls to the UI and update the back-end so that additional metadata (such as point name) can be saved and retrieved.
