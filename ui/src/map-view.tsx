import * as React from "react"
import {createRef, useCallback, useEffect, useState} from "react"
import {Feature, Map, MapBrowserEvent, View} from "ol";
import {fromLonLat} from "ol/proj";
import TileLayer from "ol/layer/Tile";
import {Cluster, OSM, Vector} from "ol/source";

import "ol/ol.css";
import {FeatureLike} from "ol/Feature";
import VectorLayer from "ol/layer/Vector";
import {GeoJSON} from "ol/format";
import {Circle, Fill, Stroke, Text, Style} from "ol/style";
import {Coordinate} from "ol/coordinate";

const geoJson = new GeoJSON()

const styleCache: { [key:string]: Style }  = {};

const getDynamicStyle = (feature: FeatureLike) => {
    const size = feature.get('features').length;
    let style = styleCache[size.toString()];
    if (!style) {
        style = new Style({
            image: new Circle({
                radius: 14,
                stroke: new Stroke({
                    color: '#000000',
                }),
                fill: new Fill({
                    color: '#509a48',
                }),
            }),
            text: new Text({
                text: size.toString(),
                fill: new Fill({
                    color: '#fff',
                }),
            }),
        });
        styleCache[size] = style;
    }
    return style;
};

const modalStyle: React.CSSProperties = {
    position: "absolute",
    top: "0px",
    left: "15px",
    height: "460px",
    width: "460px",
    margin: "20px",
    backgroundColor: "#FFFFFF",
    borderColor: "#CCCCCC",
    borderWidth: "1px",
    borderStyle: "solid",
    textAlign: "center"
};

const defaultButtonStyle: React.CSSProperties = {
    margin: "20px 10px",
    backgroundColor: "#dc3545",
    color: "#FFFFFF",
    borderRadius: "10px",
    padding: "5px 20px",
    minWidth: "100px",
}

export const MapView: React.FC = () => {
    const [map, setMap] = useState<Map | undefined>(undefined)
    const [featureLayer, setFeatureLayer] = useState<VectorLayer | undefined>()
    const [features, setFeatures] = useState<FeatureLike[]>([])
    // This can be done wih different approaches but for the sake of this exercise
    // I am going with the simple case where we keep the state of points to be added
    // as an intermediate. If there are points to be added, the UI will display a modal
    // to enter a point name.
    const [pointToBeAdded, setPointToBeAdded] = useState<Coordinate | undefined>()

    const pointLabelInputElement = createRef<HTMLInputElement>();

    useEffect(() => {
        const map = new Map({
            target: "map",
            layers: [
                new TileLayer({
                    source: new OSM()
                })
            ],
            view: new View({
                center: fromLonLat([-0.023758, 51.547504]),
                zoom: 13,
                minZoom: 6,
                maxZoom: 18
            })
        })
        map.on("click", onMapClick);

        setMap(map)
        loadFeatureData()
    }, [])

    useEffect(() => {
        if (map) {
            setFeatureLayer(addFeatureLayer(featureLayer, features))
        }
    }, [map, features])

    const loadFeatureData = () => {
        fetch("/api/geo-json")
            .then(response => response.json())
            .then(json => setFeatures(geoJson.readFeatures(json)))
    }

    const addFeatureLayer = (previousLayer: VectorLayer, features: FeatureLike[]): VectorLayer => {

        // I wasn't sure if you required the numbered cluster style but I have applied the
        // dynamic text style provided in the example given.
        const newLayer = previousLayer ? previousLayer : new VectorLayer({
            style: getDynamicStyle,
        });

        if (previousLayer != undefined) {
            previousLayer.getSource().clear();
        } else {
            map.addLayer(newLayer);
        }

        (newLayer as any).tag = "features";

        const source = new Vector({
            format: geoJson,
            features: features as Feature<any>[]
        });

        // Added extra code here in order to cluster the points
        const clusteredSource = new Cluster({
            distance: 60,
            source: source
        });

        newLayer.setSource(clusteredSource);

        return newLayer;
    }

    const sendPoint = () => {
        if (!pointToBeAdded || !pointLabelInputElement || !pointLabelInputElement.current) {
            return;
        }

        const feature = {
            type: "Feature",
            geometry: {
                type: "Point",
                coordinates: pointToBeAdded
            },
            properties: {
                name: pointLabelInputElement.current.value
            }
        };

        fetch("/api/geo-json/add", {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: 'POST',
            body: JSON.stringify(feature)
        }).then(response => {
            if (response.ok) {
                loadFeatureData();
            }
        });

        setPointToBeAdded(undefined);
    };

    // Here we are using `useCallback` in order not to recreate handler functions in every render call
    const onCancelSendingPoint = useCallback(() => {
        setPointToBeAdded(undefined);
    }, []);

    const onMapClick = (e: MapBrowserEvent) => {
        // For best practice, normally you would want to check if the click
        // event is coming from a double-click or single-click action. Because
        // when user tries to zoom-in using the double-click action we shouldn't
        // be adding new markers to the map. However, since it is not in the scope
        // of this exercise and also it will introduce a tradeoff by applying
        // delays to the click events I will leave it out.
        setPointToBeAdded(e.coordinate);
    }

    return <div>
        <div id="map" style={{height: "500px", width: "500px"}}/>
        { pointToBeAdded && pointToBeAdded.length === 2 ?
            // Ideally, we could have created a new component for the modal but since, in this
            // exercise, we are not going to be reusing it for other purposes, it is
            // ok to leave it as an inline element.
            <div id="nameModal" style={modalStyle}>
                <div style={{margin: "20px auto"}}>
                    <p>You are about to add the following coordinates:</p>
                    <p>Lat: {pointToBeAdded[0]}</p>
                    <p>Lon: {pointToBeAdded[1]}</p>
                    <p>Please enter a name for the point:</p>
                    <input ref={pointLabelInputElement} placeholder="Enter Name" />
                    <br />
                    <button
                        style={defaultButtonStyle}
                        onClick={onCancelSendingPoint}>
                        Cancel
                    </button>
                    <button
                        style={ { ...defaultButtonStyle, ...{ margin: "20px 0px 20px 10px", backgroundColor: "#28a745" } }}
                        onClick={sendPoint}>
                        Add
                    </button>
                </div>
            </div>
            :
            null
        }
    </div>
}