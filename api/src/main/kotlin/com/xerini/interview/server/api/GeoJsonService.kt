@file:Suppress("unused")

package com.xerini.interview.server.api

import com.xerini.interview.util.GsonUtil
import java.io.File
import java.util.concurrent.CopyOnWriteArrayList
import javax.ws.rs.*
import javax.ws.rs.core.MediaType.APPLICATION_JSON
import javax.ws.rs.core.Response

@Path("/geo-json")
class GeoJsonService {
    private var allFeatures: List<GeoJsonFeature> = emptyList()
    private var syncFromDataFile: Boolean = true

    @GET
    @Produces(APPLICATION_JSON)
    fun getGeoJson(): GeoJsonObject {
        // This part is the code I have used before implementing the additional meta-data stretch task.
        // Here I was using an immutable list to comply with the earlier choice of data types
        // in the data classes. However, I am not a native Kotlin developer and I'd need to
        // investigate further but I suspect there might be certain speed benefits to using
        // mutable lists or making it  mutative with `CopyOnWriteArrayList` instead (especially
        // when the number of markers get too high). In other words using `add`, instead of `plus` below.
//        var geoJsonFeatures: List<GeoJsonFeature> = emptyList()
//        for (item: List<Double> in allCoordinates) {
//            geoJsonFeatures = geoJsonFeatures.plus(GeoJsonFeature(GeometryData(item)))
//        }

        // Here I have changed `allCoordinates` to `allFeatures` in order to accommodate for the
        // additional meta-data of the Feature being send from the UI.

        // No need to read from the file (since this is an expensive operation) if the file hasn't been changed
        if (syncFromDataFile) {
            val geoJsonObject: GeoJsonObject = FileOpsHandler.readFromDataFile()
            allFeatures = geoJsonObject.features
            syncFromDataFile = false
            return geoJsonObject
        }
        return GeoJsonObject(allFeatures)
    }

    @Path("/add")
    @POST
    @Consumes(APPLICATION_JSON)
    fun addPoint(feature: GeoJsonFeature): Response {
        // Since this is a user-driven application there is no need to check if the given
        // coordinates are already in the list as the user might want to add multiple markers in the same
        // coordinates. If this was a requirement, we could have used a distinction operation to remove
        // replications, or even better, not add them in the first place.
        allFeatures = allFeatures.plus(feature)
        // Creating the geoJson object and writing to the file system
        FileOpsHandler.writeToDataFile(GeoJsonObject(allFeatures))
        // set the flag so that next time features are requested, system will sync from the data file
        syncFromDataFile = true
        return Response.ok().build()
    }
}

data class GeoJsonObject(val features: List<GeoJsonFeature>) {
    val type: String = "FeatureCollection"
}

data class GeoJsonFeature(val geometry: GeometryData?, val properties: Map<String, Any?> = emptyMap()) {
    val type: String = "Feature"
}

data class GeometryData(val coordinates: List<Double>) {
    val type: String = "Point"
}

class FileOpsHandler {

    companion object {
        val instance = FileOpsHandler()
        // The location is set to the root at the moment but obviously in production
        // this would have to change and organised under a data folder if a database
        // is not being used at that point.
        private val fileRep: File = File("GeoJsonData.json")

        fun readFromDataFile(): GeoJsonObject {
            if(!fileRep.exists()) {
                writeToDataFile(GeoJsonObject(emptyList()))
            }
            return GsonUtil.gson.fromJson<GeoJsonObject>(fileRep.readText(), GeoJsonObject::class.java)
        }
        fun writeToDataFile(geoJsonObject: GeoJsonObject) {
            fileRep.writeText(GsonUtil.gson.toJson(geoJsonObject))
        }
    }
}